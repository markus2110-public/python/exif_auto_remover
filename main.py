import os
from glob import glob
from time import sleep
from exif import Image

watch_time: int = 1

watch_dir: dir = [
    './Test_Dir2/',
    './test_dir/',
    './demo_dir/'
]

processed_files = set()


def find_files(directory: str) -> set:
    """
    :param directory:
    :return:
    """
    file_set = set()
    for file in glob(directory + "/**/*", recursive=True):
        if os.path.isfile(file) and file not in processed_files:
            print(f"New file {file}")
            file_set.add(file)

    return file_set


def process_files(file_list: set) -> None:
    for file in file_list:
        print(f"Processing {file}")
        try:
            with open(file, "rb") as f:
                file_data = Image(f)

            if file_data.has_exif:
                print(f"Removing EXIF Data {file}")
                file_data.delete_all()

                with open(file, 'wb') as new_file:
                    new_file.write(file_data.get_file())
        except Exception as e:
            print(f"ERROR: Removing EXIF Data for {file}")

        processed_files.add(file)


def run():
    try:
        while True:
            for directory in watch_dir:
                path = os.path.abspath(directory)
                if os.path.isdir(path) and os.path.exists(path):
                    process_files(find_files(path))
                    sleep(watch_time)

    except KeyboardInterrupt as e:
        print("Exit")


if __name__ == '__main__':
    run()
