import setuptools

# required packages
requirements: list = [
    "exif==1.6.0",
    "plum-py==0.8.7"

]
# test packages
test_requirements = requirements + ['flake8', 'coverage', 'tox', 'mypy']
# Dev packages
dev_requirements = test_requirements + []


with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()


setuptools.setup(
    name="sat.exif_auto_remover",
    version="0.0.1",
    author="Markus S.",
    author_email="markus@simpleasthat.de",
    description="Python module to watch folders and remove automatically the EXIF-Data for new files",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/markus2110-public/python/exif_auto_remover",
    packages=setuptools.find_packages(),
    install_requires=requirements,
    extras_require={
        'testing': test_requirements,
        'dev': dev_requirements,
    },
    python_requires='>=3.8',
    entry_points={
        'console_scripts': [
            'exif_watch = main:run',
        ]
    }

    # classifiers=[
    #     "Programming Language :: Python :: 3",
    #     "License :: OSI Approved :: MIT License",
    #     "Operating System :: OS Independent",
    # ],
    # scripts=['bin/python_package'],

    # },
)
